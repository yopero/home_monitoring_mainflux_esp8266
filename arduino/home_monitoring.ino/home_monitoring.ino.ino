#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <NTPClient.h> 
#include <WiFiUdp.h>
#include "DHTesp.h"
#include <Arduino.h>

const char *ssid     = "";
const char *password = "";
const char* mqttServer = "";
const int mqttPort = 1883;
const char* mqttUser = "6a0921b5-edae-4025-b20e-aa2c40865fa3";
const char* mqttPassword = "5e3d9c8f-cc14-4c3f-b0bc-bd14370927df";
const char* mqtt_publish_to_channel = "7781b3b1-3d2e-4bbf-b303-2a6f23029859";

#define LENG 31   //0x42 + 31 bytes equal to 32 bytes
unsigned char buf[LENG];

int PM01Value=0;          //define PM1.0 value of the air detector module
int PM2_5Value=0;         //define PM2.5 value of the air detector module
int PM10Value=0;         //define PM10 value of the air detector module 

WiFiClient espClient;
WiFiUDP ntpUDP;
PubSubClient client(espClient);
NTPClient timeClient(ntpUDP); 
DHTesp dht;

void setup() {
 
  Serial.begin(9600);
  Serial.setTimeout(1500);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    // Serial.println("Connecting to WiFi..");
  }
//   Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  timeClient.begin();
 
  while (!client.connected()) {
    // Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {

//       Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
}
 
void loop() {
 const unsigned long fiveMinutes = 5 * 60 * 1000UL;
 static unsigned long lastSampleTime = 0 - fiveMinutes;  // initialize such that a reading is due the first time through loop()

 unsigned long now = millis();
 if (now - lastSampleTime >= fiveMinutes)
 {
    lastSampleTime += fiveMinutes;
    // add code to take temperature reading here
    //  -----------------SETTING CHANNEL------------------------------------------
    char topic[100];
    sprintf(topic, "channels/%s/messages", mqtt_publish_to_channel);
//     Serial.println(topic);

    //  --------------------STARTING DHT22--------------------
    dht.setup(2, DHTesp::DHT22); // Connect DHT sensor to GPIO 2
    delay(dht.getMinimumSamplingPeriod());
    
    //  --------------------HUMIDITY--------------------
    char message[100];
    const char* measurement = "humidity";
    const char* unit = "%";
    float humidity = dht.getHumidity();

    timeClient.update();
    unsigned long timee;
    timee = timeClient.getEpochTime();
    sprintf(message, "[{\"bt\":%u,\"n\":\"\%s\",\"u\":\"\%s\",\"v\":%.1f}]", timee, measurement, unit, humidity);
//     Serial.println(message);
    
    client.publish(topic, message);
    //  --------------------TEMPERATURE--------------------
    message[100];
    measurement = "temperature";
    unit = "C";
    float temperature = dht.getTemperature();

    timeClient.update();
    timee = timeClient.getEpochTime();
    sprintf(message, "[{\"bt\":%u,\"n\":\"\%s\",\"u\":\"\%s\",\"v\":%.1f}]", timee, measurement, unit, temperature);
    // Serial.println(topic);
    // Serial.println(message);
    client.publish(topic, message);
    if(Serial.find(0x42)){    //start to read when detect 0x42
        Serial.readBytes(buf,LENG);

        if(buf[0] == 0x4d){
        if(checkValue(buf,LENG)){
            PM01Value=transmitPM01(buf); //count PM1.0 value of the air detector module
            PM2_5Value=transmitPM2_5(buf);//count PM2.5 value of the air detector module
            PM10Value=transmitPM10(buf); //count PM10 value of the air detector module 
        }           
        } 
    }
    //  --------------------pm1.0--------------------
    message[100];
    measurement = "PM1.0";
    unit = "ug/m3";

    timeClient.update();
    timee = timeClient.getEpochTime();
    sprintf(message, "[{\"bt\":%u,\"n\":\"\%s\",\"u\":\"\%s\",\"v\":%u}]", timee, measurement, unit, PM01Value);
    // Serial.println(topic);
    // Serial.println(message);
    client.publish(topic, message);
    
    //  --------------------pm2.5--------------------
    measurement = "PM2.5";
    timeClient.update();
    timee = timeClient.getEpochTime();
    
    sprintf(message, "[{\"bt\":%u,\"n\":\"\%s\",\"u\":\"\%s\",\"v\":%u}]", timee, measurement, unit, PM2_5Value);
//    Serial.println(topic);
//    Serial.println(message);
    client.publish(topic, message);

    //  --------------------pm10--------------------
    measurement = "PM10";
    timeClient.update();
    timee = timeClient.getEpochTime();
    
    sprintf(message, "[{\"bt\":%u,\"n\":\"\%s\",\"u\":\"\%s\",\"v\":%u}]", timee, measurement, unit, PM10Value);
    // Serial.println(topic);
    // Serial.println(message);
    client.publish(topic, message);
    
 }
 client.loop(); 
}

char checkValue(unsigned char *thebuf, char leng)
{  
  char receiveflag=0;
  int receiveSum=0;

  for(int i=0; i<(leng-2); i++){
  receiveSum=receiveSum+thebuf[i];
  }
  receiveSum=receiveSum + 0x42;
 
  if(receiveSum == ((thebuf[leng-2]<<8)+thebuf[leng-1]))  //check the serial data 
  {
    receiveSum = 0;
    receiveflag = 1;
  }
  return receiveflag;
}
int transmitPM01(unsigned char *thebuf)
{
  int PM01Val;
  PM01Val=((thebuf[3]<<8) + thebuf[4]); //count PM1.0 value of the air detector module
  return PM01Val;
}
//transmit PM Value to PC
int transmitPM2_5(unsigned char *thebuf)
{
  int PM2_5Val;
  PM2_5Val=((thebuf[5]<<8) + thebuf[6]);//count PM2.5 value of the air detector module
  return PM2_5Val;
  }
//transmit PM Value to PC
int transmitPM10(unsigned char *thebuf)
{
  int PM10Val;
  PM10Val=((thebuf[7]<<8) + thebuf[8]); //count PM10 value of the air detector module  
  return PM10Val;
}
