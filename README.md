
# Environmental Monitoring System

<img src="mainflux.jpeg"  width="360" height="800">

- Mainflux
- ESP8266 microcontoller
- PMS5003 sensor
- AM2120
- Digital Ocean Droplet

# Docker Composition

Configure environment variables and run Mainflux Docker Composition.

*Note**: `docker-compose` uses `.env` file to set all environment variables. Ensure that you run the command from the same location as .env file.

## Installation

Follow the [official documentation](https://docs.docker.com/compose/install/).


## Usage

Run following commands from project root directory.

```
docker-compose -f docker/docker-compose.yml up
```

```
docker-compose -f docker/addons/<path>/docker-compose.yml  up
```
